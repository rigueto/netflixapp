<?php
//Incluir a conexão com banco de dados
include_once '../simple/config.php';

$search = filter_input(INPUT_POST, 'userSearch', FILTER_SANITIZE_STRING);

// When search button is used.
if (strpos($search, ':>') !== false) {
	$splitArray = explode(":> ", $search);

	$type = $splitArray[0];
	$search = $splitArray[1];

	if($search !== '') {
		if($type === 'Title') {

			$result_user = "select title, runtime, genre, yearMovie, mvtv, country, imdb, rottenRating, metascore, directors, actors, plot, awards, id, imgOmdb from netflix_movie_series where title like '%$search%' order by imdb DESC;";

		}elseif($type === 'Director') {

			$result_user = "select title, runtime, genre, yearMovie, mvtv, country, imdb, rottenRating, metascore, directors, actors, plot, awards, id, imgOmdb from netflix_movie_series where directors like '%$search%' order by imdb DESC;";

		}elseif($type === 'Actor') {

			$result_user = "select title, runtime, genre, yearMovie, mvtv, country, imdb, rottenRating, metascore, directors, actors, plot, awards, id, imgOmdb from netflix_movie_series where actors like '%$search%' order by imdb DESC;";
		}
	}else{
		$result_user = "select id from netflix_movie_series where title = 'nonono';";
	}

// When top ranking is used.
} else {
	if($search === 'Top 200 Movies') {

		$result_user = "select title, runtime, genre, yearMovie, mvtv, country, imdb, rottenRating, metascore, directors, actors, plot, awards, id, imgOmdb from netflix_movie_series where mvtv = 'movie' and imdb >= 70 order by imdb DESC limit 200;";

	}elseif($search === 'Top Movie Actions') {

		$result_user = "select title, runtime, genre, yearMovie, mvtv, country, imdb, rottenRating, metascore, directors, actors, plot, awards, id, imgOmdb from netflix_movie_series where mvtv = 'movie' and imdb >= 70 and genre like '%Action%' order by imdb DESC limit 200;";

	}elseif($search === 'Top Movie Comedy (~105min)') {

		$result_user = "select title, runtime, genre, yearMovie, mvtv, country, imdb, rottenRating, metascore, directors, actors, plot, awards, id, imgOmdb from netflix_movie_series where mvtv = 'movie' and imdb >= 70 and genre like '%Comedy%' and runtime <= 105 and runtime <> 'N/A' order by imdb DESC limit 200;";

	
		
	}elseif($search === 'Top 200 TV Show') {

		$result_user = "select title, runtime, genre, yearMovie, mvtv, country, imdb, rottenRating, metascore, directors, actors, plot, awards, id, imgOmdb from netflix_movie_series where mvtv = 'series' and imdb >= 70 order by imdb DESC limit 200;";

	}elseif($search === 'Top TV Show Actions') {

		$result_user = "select title, runtime, genre, yearMovie, mvtv, country, imdb, rottenRating, metascore, directors, actors, plot, awards, id, imgOmdb from netflix_movie_series where mvtv = 'series' and imdb >= 70 and genre like '%Action%' order by imdb DESC limit 200;";

	}elseif($search === 'Top TV Show Comedy') {

		$result_user = "select title, runtime, genre, yearMovie, mvtv, country, imdb, rottenRating, metascore, directors, actors, plot, awards, id, imgOmdb from netflix_movie_series where mvtv = 'series' and imdb >= 70 and genre like '%Comedy%' order by imdb DESC limit 200;";

	}


}


$resultado_user = $conn->query($result_user);
$resultRows = $resultado_user->rowCount();
 
if(($resultado_user) && ($resultRows != 0)){
	
	while($rows = $resultado_user->fetch(PDO::FETCH_ASSOC)){

		echo "rtn:".$rows['title']."
			  rtn:".$rows['runtime']."
			  rtn:".$rows['genre']."
			  rtn:".$rows['yearMovie']."
			  rtn:".$rows['mvtv']."
			  rtn:".$rows['country']."
			  rtn:".$rows['imdb']."
			  rtn:".$rows['rottenRating']."
			  rtn:".$rows['metascore']."
			  rtn:".$rows['directors']."
			  rtn:".$rows['actors']."			  
			  rtn:".$rows['plot']."
			  rtn:".$rows['awards']."
			  rtn:".$rows['id']."
			  
			  rtn:".$rows['imgNetflix'].":row:";
			  
			  //rtn:".$rows['imgOmdb']."
 
	}

}else{
	echo "$search";
}
 