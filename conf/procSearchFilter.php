<?php
//Incluir a conexão com banco de dados
include_once '../simple/config.php';

$searchFilter = filter_input(INPUT_POST, 'userSearchFilter', FILTER_SANITIZE_STRING);

$splitArray = explode(",", $searchFilter);

$genre = 		$splitArray[0];
$runtime = 		$splitArray[1];
$yearMovie = 	$splitArray[2];
$mvtv = 		$splitArray[3];
$country = 		$splitArray[4];
$imdb = 		$splitArray[5];
$rottenRating = $splitArray[6];
$metascore = 	$splitArray[7];

if($genre != 'any') {
  	$genre =  " genre like '%$genre%'";
}else{
	$genre = '';
}

if($runtime != 'any') {
	$runtimeArr = explode("~", $runtime);

	if(is_numeric($runtimeArr[0])) {
		$runtimeIni = $runtimeArr[0];
	} else {
		$runtime = 'any';
	}
	if(is_numeric($runtimeArr[1])) {
		$runtimeFin = $runtimeArr[1];

		$runtime = " and runtime between $runtimeIni and $runtimeFin";
	} 
}else{
	$runtime = '';
}

if($yearMovie != 'any') {
	$yearMovie =  " and yearMovie like '%$yearMovie%'";
}else{
	$yearMovie = '';
}

if($mvtv != 'any') {
	$mvtv =  " and mvtv like '%$mvtv%'";
}else{
	$mvtv = '';
}

if($country != 'any') {
	$country = " and country like '%$country%'";
}else{
	$country = '';
}

if($imdb != 'any') {

	if($imdb === '50') {
		$imdb = " and imdb <= $imdb and imdb <> 'N/A' ";
	}else{
		$imdb = " and imdb >= $imdb and imdb <> 'N/A' ";
	}
	
	// $imdbArr = explode("~", $imdb);

	// if(is_numeric($imdbArr[0])) {
	// 	$imdbIni = $imdbArr[0];
	// } else {
	// 	$imdb = 'any';
	// }
	// if(is_numeric($imdbArr[1])) {
	// 	$imdbFin = $imdbArr[1];
	// 	$imdb = " and imdb between '$imdbIni' and '$imdbFin' and imdb <> 'N/A' ";
	// } 
}else{
	$imdb = '';
}

if($rottenRating != 'any') {

	if($rottenRating === '50') {
		$rottenRating = " and rottenRating <= $rottenRating and rottenRating <> 'N/A' ";
	}else{
		$rottenRating = " and rottenRating >= $rottenRating and rottenRating <> 'N/A' ";
	}
 
	// $rottenRatingArr = explode("~", $rottenRating);

	// if(is_numeric($rottenRatingArr[0])) {
	// 	$rottenRatingIni = $rottenRatingArr[0];
	// } else {
	// 	$rottenRating = 'any';
	// }
	// if(is_numeric($rottenRatingArr[1])) {
	// 	$rottenRatingFin = $rottenRatingArr[1];

	// 	$rottenRating = " and rottenRating between '$rottenRatingIni' and '$rottenRatingFin' and rottenRating <> 'N/A'";
	// } 
}else{
	$rottenRating = '';
}

if($metascore != 'any') {

	if($metascore === '50') {
		$metascore = " and metascore <= $metascore and metascore <> 'N/A' ";
	}else{
		$metascore = " and metascore >= $metascore and metascore <> 'N/A' ";
	}
 
	
	// $metascoreArr = explode("~", $metascore);
	// if(is_numeric($metascoreArr[0])) {
	// 	$metascoreIni = $metascoreArr[0];
	// } else {
	// 	$metascore = 'any';
	// }
	// if(is_numeric($metascoreArr[1])) {
	// 	$metascoreFin = $metascoreArr[1];

	// 	$metascore = " and metascore between '$metascoreIni' and '$metascoreFin' and metascore <> 'N/A'";
	// } 
}else{
	$metascore = '';
}
 
if($genre === '' && $runtime === '' && $yearMovie === '' && $mvtv === '' && $country === '' && $imdb === '' && $rottenRating === '' && $metascore === '') {
	//$result_user_filter = "select * from netflix_movie_series  limit 1;";
	$result_user_filter = "select * from netflix_movie_series where imdb <> 'N/A' order by imdb DESC limit 200 ";

}else{
	$queryInitial = "select title, runtime, genre, yearMovie, mvtv, country, imdb, rottenRating, metascore, directors, actors, plot, awards, id, imgOmdb from netflix_movie_series where $genre $runtime $yearMovie $mvtv $country $imdb $rottenRating $metascore order by imdb DESC limit 200";
	$arrayWheAnd = array('where and', 'where  and', 'where   and', 'where    and','where     and', 'where      and', 'where       and', 'where        and' , 'where         and');
	$result_user_filter = str_replace($arrayWheAnd, 'where ', $queryInitial);
}

//Consulta Query.
$resultado_user = $conn->query($result_user_filter);
$resultRows = $resultado_user->rowCount();
 
if(($resultado_user) && ($resultRows != 0)){
	
	while($rows = $resultado_user->fetch(PDO::FETCH_ASSOC)){
		echo "rtn:".$rows['title']."
		rtn:".$rows['runtime']."
		rtn:".$rows['genre']."
		rtn:".$rows['yearMovie']."
		rtn:".$rows['mvtv']."
		rtn:".$rows['country']."
		rtn:".$rows['imdb']."
		rtn:".$rows['rottenRating']."
		rtn:".$rows['metascore']."
		rtn:".$rows['directors']."
		rtn:".$rows['actors']."			  
		rtn:".$rows['plot']."
		rtn:".$rows['awards']."
		rtn:".$rows['id']."
		
		rtn:".$rows['imgNetflix'].":row:";
		
		//rtn:".$rows['imgOmdb']."
	}

}else{
	echo "";
}
 




// if($genre != 'any' && $runtime != 'any' && $yearMovie != 'any' && $mvtv != 'any' && $country != 'any' && $imdb != 'any' && $rottenRating != 'any' && $metascore != 'any') {
// 	$result_user_filter = "select * from netflix_movie_series where genre like '%$genre%' and
// 	runtime between '$runtimeIni' and '$runtimeFin' and yearMovie like '%$yearMovie%' and mvtv like '%$mvtv%' and
// 	country like '%$country%' and imdb between '$imdbIni' and '$imdbFin' and rottenRating between '$rottenRatingIni' and '$rottenRatingFin' and
// 	metascore between '$metascoreIni' and '$metascoreFin';";

// }elseif($genre != 'any' && $runtime != 'any' && $yearMovie != 'any' && $mvtv != 'any' && $country != 'any' && $imdb != 'any' && $rottenRating != 'any') {
// 	$result_user_filter = "select * from netflix_movie_series where genre like '%$genre%' and
// 	runtime between '$runtimeIni' and '$runtimeFin' and yearMovie like '%$yearMovie%' and mvtv like '%$mvtv%' and
// 	country like '%$country%' and imdb between '$imdbIni' and '$imdbFin' and rottenRating between '$rottenRatingIni' and '$rottenRatingFin';";

// }elseif($genre != 'any' && $runtime != 'any' && $yearMovie != 'any' && $mvtv != 'any' && $country != 'any' && $imdb != 'any') {
// 	$result_user_filter = "select * from netflix_movie_series where genre like '%$genre%' and
// 	runtime between '$runtimeIni' and '$runtimeFin' and yearMovie like '%$yearMovie%' and mvtv like '%$mvtv%' and
// 	country like '%$country%' and imdb between '$imdbIni' and '$imdbFin';";

// }elseif($genre != 'any' && $runtime != 'any' && $yearMovie != 'any' && $mvtv != 'any' && $country != 'any') {
// 	$result_user_filter = "select * from netflix_movie_series where genre like '%$genre%' and
// 	runtime between '$runtimeIni' and '$runtimeFin' and yearMovie like '%$yearMovie%' and mvtv like '%$mvtv%' and
// 	country like '%$country%';";

// }elseif($genre != 'any' && $runtime != 'any' && $yearMovie != 'any' && $mvtv != 'any') {
// 	$result_user_filter = "select * from netflix_movie_series where genre like '%$genre%' and
// 	runtime between '$runtimeIni' and '$runtimeFin' and yearMovie like '%$yearMovie%' and mvtv like '%$mvtv%';";

// }elseif($genre != 'any' && $runtime != 'any' && $yearMovie != 'any') {
// 	$result_user_filter = "select * from netflix_movie_series where genre like '%$genre%' and
// 	runtime between '$runtimeIni' and '$runtimeFin' and yearMovie like '%$yearMovie%';";

// }elseif($genre != 'any' && $runtime != 'any') {
// 	$result_user_filter = "select * from netflix_movie_series where genre like '%$genre%' and
// 	runtime between '$runtimeIni' and '$runtimeFin';";
	
// }elseif($genre !=  'any') {
// 	$result_user_filter = "select * from netflix_movie_series where genre like '%$genre%';";

// }else{
// 	$result_user_filter = "select * from netflix_movie_series limit 50;";
// }



// if($runtime != 'any') {
// 	$runtimeArr = explode("~", $runtime);

// 	if(is_numeric($runtimeArr[0])) {
// 		$runtimeIni = $runtimeArr[0];
// 	} else {
// 		$runtime = 'any';
// 	}
// 	if(is_numeric($runtimeArr[1])) {
// 		$runtimeFin = $runtimeArr[1];
// 	} 
// }

// if($imdb != 'any') {
// 	$imdbArr = explode("~", $imdb);

// 	if(is_numeric($imdbArr[0])) {
// 		$imdbIni = $imdbArr[0];
// 	} else {
// 		$imdb = 'any';
// 	}
// 	if(is_numeric($imdbArr[1])) {
// 		$imdbFin = $imdbArr[1];
// 	} 
// }
// $rottenRating = $splitArray[6];
// if($rottenRating != 'any') {
// 	$rottenRatingArr = explode("~", $rottenRating);

// 	if(is_numeric($rottenRatingArr[0])) {
// 		$rottenRatingIni = $rottenRatingArr[0];
// 	} else {
// 		$rottenRating = 'any';
// 	}
// 	if(is_numeric($rottenRatingArr[1])) {
// 		$rottenRatingFin = $rottenRatingArr[1];
// 	} 
// }
// $metascore = $splitArray[7];
// if($metascore != 'any') {
// 	$metascoreArr = explode("~", $metascore);

// 	if(is_numeric($metascoreArr[0])) {
// 		$metascoreIni = $metascoreArr[0];
// 	} else {
// 		$metascore = 'any';
// 	}
// 	if(is_numeric($metascoreArr[1])) {
// 		$metascoreFin = $metascoreArr[1];
// 	} 
// }


// if($genre !=  'any' && $runtime == 'any' || $genre !=  'any' && $yearMovie == 'any' || $genre !=  'any' && $mvtv == 'any' || $genre !=  'any' && $country == 'any'
// || $genre !=  'any' && $imdb == 'any' || $genre !=  'any' && $rottenRating == 'any' || $genre !=  'any' && $metascore == 'any') {
// 	$result_user_filter = "select * from netflix_movie_series where genre like '%$genre%';";
// }elseif($genre != 'any' && $runtime != 'any') {
// 	$result_user_filter = "select * from netflix_movie_series where genre like '%$genre%' and
// 	runtime between '$runtimeIni' and '$runtimeFin';";
// }


// if($genre != 'any' && $runtime != 'any' && $yearMovie != 'any' && $mvtv != 'any' && $country != 'any' && $imdb != 'any' && $rottenRating != 'any' && $metascore != 'any') {
// 	$result_user_filter = "select * from netflix_movie_series where genre like '%$genre%' and
// 	runtime between '$runtimeIni' and '$runtimeFin' and yearMovie like '%$yearMovie%' and mvtv like '%$mvtv%' and
// 	country like '%$country%' and imdb between '$imdbIni' and '$imdbFin' and rottenRating between '$rottenRatingIni' and '$rottenRatingFin' and
// 	metascore between '$metascoreIni' and '$metascoreFin';";

// }elseif($genre != 'any' && $runtime != 'any' && $yearMovie != 'any' && $mvtv != 'any' && $country != 'any' && $imdb != 'any' && $rottenRating != 'any') {
// 	$result_user_filter = "select * from netflix_movie_series where genre like '%$genre%' and
// 	runtime between '$runtimeIni' and '$runtimeFin' and yearMovie like '%$yearMovie%' and mvtv like '%$mvtv%' and
// 	country like '%$country%' and imdb between '$imdbIni' and '$imdbFin' and rottenRating between '$rottenRatingIni' and '$rottenRatingFin';";

// }elseif($genre != 'any' && $runtime != 'any' && $yearMovie != 'any' && $mvtv != 'any' && $country != 'any' && $imdb != 'any') {
// 	$result_user_filter = "select * from netflix_movie_series where genre like '%$genre%' and
// 	runtime between '$runtimeIni' and '$runtimeFin' and yearMovie like '%$yearMovie%' and mvtv like '%$mvtv%' and
// 	country like '%$country%' and imdb between '$imdbIni' and '$imdbFin';";

// }elseif($genre != 'any' && $runtime != 'any' && $yearMovie != 'any' && $mvtv != 'any' && $country != 'any') {
// 	$result_user_filter = "select * from netflix_movie_series where genre like '%$genre%' and
// 	runtime between '$runtimeIni' and '$runtimeFin' and yearMovie like '%$yearMovie%' and mvtv like '%$mvtv%' and
// 	country like '%$country%';";

// }elseif($genre != 'any' && $runtime != 'any' && $yearMovie != 'any' && $mvtv != 'any') {
// 	$result_user_filter = "select * from netflix_movie_series where genre like '%$genre%' and
// 	runtime between '$runtimeIni' and '$runtimeFin' and yearMovie like '%$yearMovie%' and mvtv like '%$mvtv%';";

// }elseif($genre != 'any' && $runtime != 'any' && $yearMovie != 'any') {
// 	$result_user_filter = "select * from netflix_movie_series where genre like '%$genre%' and
// 	runtime between '$runtimeIni' and '$runtimeFin' and yearMovie like '%$yearMovie%';";

// }elseif($genre != 'any' && $runtime != 'any') {
// 	$result_user_filter = "select * from netflix_movie_series where genre like '%$genre%' and
// 	runtime between '$runtimeIni' and '$runtimeFin';";
	
// }elseif($genre !=  'any') {
// 	$result_user_filter = "select * from netflix_movie_series where genre like '%$genre%';";

// }else{
// 	$result_user_filter = "select * from netflix_movie_series limit 50;";
// }





// if($genre != 'Any') {
//   	$genre =  " genre like '%$genre% ";
// }else{
// 	$genre = '';
// }

// if($runtime != 'Any') {
// 	$runtimeArr = explode("~", $runtime);

// 	if(is_numeric($runtimeArr[0])) {
// 		$runtimeIni = $runtimeArr[0];
// 	} else {
// 		$runtime = 'Any';
// 	}
// 	if(is_numeric($runtimeArr[1])) {
// 		$runtimeFin = $runtimeArr[1];

// 		$runtime = " and runtime between '$runtimeIni' and '$runtimeFin' ";
// 	} 
// }else{
// 	$runtime = '';
// }

// if($yearMovie != 'Any') {
// 	$yearMovie =  " and yearMovie like '%$yearMovie%' ";
// }else{
// 	$yearMovie = '';
// }

// if($mvtv != 'Any') {
// 	$mvtv =  " and mvtv like '%$mvtv%' ";
// }else{
// 	$mvtv = '';
// }

// if($country != 'Any') {
// 	$country = " and country like '%$country%' ";
// }else{
// 	$country = '';
// }

// if($imdb != 'Any') {
// 	$imdbArr = explode("~", $imdb);

// 	if(is_numeric($imdbArr[0])) {
// 		$imdbIni = $imdbArr[0];
// 	} else {
// 		$imdb = 'Any';
// 	}
// 	if(is_numeric($imdbArr[1])) {
// 		$imdbFin = $imdbArr[1];
// 		$imdb = " and imdb between '$imdbIni' and '$imdbFin' ";
// 	} 
// }else{
// 	$imdb = '';
// }

// if($rottenRating != 'Any') {
// 	$rottenRatingArr = explode("~", $rottenRating);

// 	if(is_numeric($rottenRatingArr[0])) {
// 		$rottenRatingIni = $rottenRatingArr[0];
// 	} else {
// 		$rottenRating = 'Any';
// 	}
// 	if(is_numeric($rottenRatingArr[1])) {
// 		$rottenRatingFin = $rottenRatingArr[1];

// 		$rottenRating = " and rottenRating between '$rottenRatingIni' and '$rottenRatingFin' ";
// 	} 
// }else{
// 	$rottenRating = '';
// }

// if($metascore != 'Any') {
// 	$metascoreArr = explode("~", $metascore);

// 	if(is_numeric($metascoreArr[0])) {
// 		$metascoreIni = $metascoreArr[0];
// 	} else {
// 		$metascore = 'Any';
// 	}
// 	if(is_numeric($metascoreArr[1])) {
// 		$metascoreFin = $metascoreArr[1];

// 		$metascore = " and metascore between '$metascoreIni' and '$metascoreFin' ";
// 	} 
// }else{
// 	$metascore = '';
// }