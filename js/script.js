$(function(){
	//Get click of btn search and call function findmovie.
	$(".searchBtn").click(function(){		
		//Active loading.
		$(".spinner").css("display", "block");
		const searchBtn = 'searchBtn';
		findMovie(searchBtn);
	})

	// Behaviors to transform hamburguer menu.
	$("#menuToggle").click(function(){
		//event.preventDefault();
		$("#menu").css("transform", "none");
		$("#menuToggle > span:nth-child(2)").css("transform", "rotate(45deg) translate(-2px, -1px)");
		$("#menuToggle > span:nth-child(2)").css("opacity", "1");
		$("#menuToggle > span:nth-child(3)").css("transform", "rotate(0deg) scale(0.2, 0.2)");
		$("#menuToggle > span:nth-child(3)").css("opacity", "0");
		$("#menuToggle > span:nth-child(4)").css("transform", "rotate(-45deg) translate(0, -1px)");

		if($("#menu").hasClass("show")) {
			$("#menu").css("transform", "translate(-250%, 0)");
			$("#menu").removeClass("show");
			resetHamburguer();
		}else {
			$("#menu").addClass("show");
		}
	})	

	// Reset hamburguer on li click. 
	$("#menu > li").click(function(){
		resetHamburguer();
	})	

	// Get top ranking hamburguer. 
	$("#menu > li").click(function(){
		//Active loading.
		$(".spinner").css("display", "block");
		const topSelected = $(this).text();
		findMovie(topSelected);
	})		

});

// Restore hamburguer when li click.
function resetHamburguer(){
	$("#menuToggle span").css("opacity", "1");
	$("#menuToggle span").css("transform", "none");
}

//Function to search movie btn.
function findMovie(searchOrRanking) {

	// Disable click when search is loading.
	$('.searchBtn').attr('disabled','disabled');
	$('.filterMaster').attr('disabled','disabled');
	$('.checkbox').attr('disabled','disabled');

	// Reset fields search and adjust footer.
	$(".selectpicker").val('any');
	$(".selectpicker").selectpicker("refresh");

	if(searchOrRanking === 'searchBtn') {
		const optionSolo = $('.custom-option-top.selected').text();
		const search = $('#inputAutoComp').val();
		
		var searchFull = optionSolo + '> ' + search;
 
	} else {
		var searchFull = searchOrRanking;	
	}


	const data = {
		userSearch : searchFull
	}
	$.post('conf/procSearch.php', data, function(callback){

		callRows(callback);

	});
 
}
 

//Function to filter movie.
$(function(){
 
	$(".filterMaster").click(function(){
		//Active loading.
		$(".spinner").css("display", "block");

		// Disable click when search is loading.
		$('.searchBtn').attr('disabled','disabled');
		$('.filterMaster').attr('disabled','disabled');
		$('.checkbox').attr('disabled','disabled');

		//Clear search button and select.
		$('.custom-select__trigger-top > span').text('Title:');
		$('.custom-options-top > span:nth-child(1)').addClass('selected');
		$('.custom-options-top > span:nth-child(2)').removeClass('selected');
		$('.custom-options-top > span:nth-child(3)').removeClass('selected');
		//Clear inputsearch.
		$('#inputAutoComp').val('');		

		//Get all selects value.
		const allSelects = $('.custom-options').map((_,el) => el.value).get();

		const dataFilter = {
			userSearchFilter : allSelects.toString()
		}
		
		$.post('conf/procSearchFilter.php', dataFilter, function(callback){
			
			callRows(callback);

		});
	});
});

//Submit search when user press enter.
$("#inputAutoComp").keypress(function(event) {
	if (event.keyCode === 13) {
		const searchBtn = 'searchBtn';
		findMovie(searchBtn);
	}
});


//Function to call all results.
function callRows(callback) {

	// Remove dom.
	const containerInit = document.getElementById('container')
	containerInit.parentNode.removeChild(containerInit);
 
	const app = document.getElementById('root');	
	const content = document.createElement('content');
	content.setAttribute('class', 'content');
	content.setAttribute('id', 'content');
 
	// const logo = document.createElement('img');
	// logo.src = 'logo.png';
	const container = document.createElement('div');
	
	container.setAttribute('class', 'container');
	container.setAttribute('id', 'container');
	//app.appendChild(logo);
	app.appendChild(container);


	let splitRows = '';
	let results = '';
	if (callback.includes(":row:")) { 
        
		splitRows = callback.split(':row:');
		
		const countResultsInit = document.getElementById('countResults')
		if(typeof countResultsInit !== 'undefined') {
			countResultsInit.parentNode.removeChild(countResultsInit);
		}

		const countResults = document.createElement('div');
		countResults.setAttribute('class', 'countResults');
		countResults.setAttribute('id', 'countResults');

		let formSearch = document.getElementById('form-search');


		results = splitRows.length -1;
		if(results === 1) { 
			countResults.textContent = 'Found 1 result.';
			formSearch.appendChild(countResults);
		}else if(results > 1) {
			countResults.textContent = 'Found '+results+' results.';
			formSearch.appendChild(countResults);
		}

		for (let index = 0; index <= (splitRows.length -2); index++) {
			
			let splitStr = splitRows[index].split('rtn:');
 
			const card = document.createElement('div');
			card.setAttribute('class', 'card notes');
			
			//Div Parent.
			const divParentDetails = document.createElement('div');
			divParentDetails.setAttribute('class', 'divParentDetails');

			//Div to 1 floor notes.
			const divNotes = document.createElement('div');
			divNotes.setAttribute('class', 'divNotes');
	  
			//Div to 2 floor.
			const divYrTyp = document.createElement('div');
			divYrTyp.setAttribute('class', 'divYrTyp');
			
			//Div to 3 floor.
			const divCountry = document.createElement('div');
			divCountry.setAttribute('class', 'divCountry');
	  
			//Div to 4 floor.
			const divOscar = document.createElement('div');
			divOscar.setAttribute('class', 'divOscar');
	  
			//Div to 5 floor More.
			const divMore = document.createElement('div');
			divMore.setAttribute('class', 'divMore');
	  
			//imd omdb
			//if(splitStr[15] !== "" && splitStr[15] !== "N/A") {
			//	//img from omdb.
			//	inititleMovie = document.createElement('img');
			//	inititleMovie.src = splitStr[15];
			//	inititleMovie.setAttribute('class', 'firstImg');
			//ntf img
			//console.log(splitStr[16]);
			
			if(splitStr[15] !== "" && splitStr[15] !== "N/A") {
				inititleMovie = document.createElement('img');
				inititleMovie.src = splitStr[15];
				inititleMovie.setAttribute('class', 'firstImg');
			} else {
				inititleMovie = document.createElement('h4');
				inititleMovie.setAttribute('class', 'initial-title');
				inititleMovie.textContent = splitStr[1].normalize('NFD').replace(/[\u0300-\u036f]/g, "");
			}
	   
			const imgRatting = document.createElement('img');
			imgRatting.src = 'icon/imdb.png'; 
			imgRatting.setAttribute('class', 'imgImdb');
	   
			const imgRotRatting = document.createElement('img');
			imgRotRatting.src = 'icon/tomatometer.svg'; 
			imgRotRatting.setAttribute('class', 'imgNotes');
	  
			const imgMatRatting = document.createElement('img');
			imgMatRatting.src = 'icon/metacritic.svg'; 
			imgMatRatting.setAttribute('class', 'imgNotes');
	   
			// Logo image of imdb, rotten e meta.
			const pImdb = document.createElement('p');
			pImdb.setAttribute('class', 'pNotes');
			pImdb.textContent = splitStr[7];
	  
			const prorottenRating = document.createElement('p');
			prorottenRating.setAttribute('class', 'pNotes');
			prorottenRating.textContent = splitStr[8];
	  
			const pmetascore = document.createElement('p');
			pmetascore.setAttribute('class', 'pNotes');
			pmetascore.textContent = splitStr[9];
	  
			const imgTime = document.createElement('img');
			imgTime.src = 'icon/time.png'; 
			imgTime.setAttribute('class', 'imgTime')
	   
			const imgYear = document.createElement('img');
			imgYear.src = 'icon/year.png'; 
			imgYear.setAttribute('class', 'imgNotes');
	  
			const imgType = document.createElement('img');
			imgType.src = 'icon/movie.png'; 
			imgType.setAttribute('class', 'imgNotes');
	  
			const pimgTime = document.createElement('p');
			pimgTime.setAttribute('class', 'pNotes');
			pimgTime.textContent = splitStr[2]+'min';
	  
			const pimgYear = document.createElement('p');
			pimgYear.setAttribute('class', 'pNotes');
			let yearMovie = splitStr[4].replace(/[^0-9]+/g, '-');

			let lastChar = yearMovie.charAt(yearMovie.length-1);
			if(lastChar === '-') {
				yearMovie = yearMovie.slice(0, -1);
			}
			pimgYear.textContent = yearMovie;
	  
			const pimgType = document.createElement('p');
			pimgType.setAttribute('class', 'pNotes');
			pimgType.textContent = splitStr[5].replace(/[^a-z]+/g, '');
	   
			const imgCountry = document.createElement('img');
			imgCountry.src = 'icon/country.png'; 
			imgCountry.setAttribute('class', 'imgNotes');
	  
			const pimgCountry = document.createElement('p');
			pimgCountry.setAttribute('class', 'pNotes');
			pimgCountry.textContent = splitStr[6];
	  
			const imgOscar = document.createElement('img');
			imgOscar.src = 'icon/oscar.png'; 
			imgOscar.setAttribute('class', 'imgNotes');
	  
			const pimgOscar = document.createElement('p');
			pimgOscar.setAttribute('class', 'pNotes');
			pimgOscar.textContent = splitStr[13];  
	  
			const imgMore = document.createElement('img');
			imgMore.src = 'icon/more.png'; 
			imgMore.setAttribute('class', 'imgNotesMore');
			imgMore.setAttribute('data-toggle', 'modal');
			imgMore.setAttribute('data-target', '#cod'+splitStr[14].replace(/[^0-9]+/g, ''));
 
			const pimgMore = document.createElement('p');
			pimgMore.setAttribute('class', 'pNotes');
			pimgMore.textContent = 'Actor, Directors and Plot...';      
	  
			container.appendChild(card);
	   
			//First img - title.
			card.appendChild(inititleMovie);
	  
			let divParent = card.appendChild(divParentDetails);
			let divNotesInter = divParent.appendChild(divNotes);

			let pAppend = divNotesInter.appendChild(pImdb);
			pAppend.appendChild(imgRatting);
	   
			let pAppendRt = divNotesInter.appendChild(prorottenRating);
			pAppendRt.appendChild(imgRotRatting);
	  
			let pAppendMt = divNotesInter.appendChild(pmetascore);
			pAppendMt.appendChild(imgMatRatting);      
	  
			let imgTimeInter = divParent.appendChild(divYrTyp);
	
			let pAppendTm = imgTimeInter.appendChild(pimgTime);
			pAppendTm.appendChild(imgTime);
			let pAppendYr = imgTimeInter.appendChild(pimgYear);
			pAppendYr.appendChild(imgYear);
			let pAppendTp = imgTimeInter.appendChild(pimgType);
			pAppendTp.appendChild(imgType);     
	  
			let imgCountryInter = divParent.appendChild(divCountry);
	  
			let pCountry = imgCountryInter.appendChild(pimgCountry);
			pCountry.appendChild(imgCountry);      
			
			let imgOscarInter = divParent.appendChild(divOscar);
	  
			let pOscar = imgOscarInter.appendChild(pimgOscar);
			pOscar.appendChild(imgOscar);    
	  
			let imgMoreInter = divParent.appendChild(divMore);
	  
			let pMore = imgMoreInter.appendChild(pimgMore);
			pMore.appendChild(imgMore);    

			// ========================================== //
			// ================modal===================== //
			let modalMore = document.createElement('div');
			modalMore.setAttribute('class', 'modal fade');
			modalMore.setAttribute('id', 'cod'+splitStr[14].replace(/[^0-9]+/g, ''));
			modalMore.setAttribute('tabindex', '-1');
			modalMore.setAttribute('role', 'dialog');
			modalMore.setAttribute('aria-labelledby', 'modalLongTitle');
			modalMore.setAttribute('aria-hidden', 'true');

			let modalMoreL2 = document.createElement('div');
			modalMoreL2.setAttribute('class', 'modal-dialog');
			modalMoreL2.setAttribute('role', 'document');			
			
			let modalMoreL3 = document.createElement('div');
			modalMoreL3.setAttribute('class', 'modal-content');
 	
			let modalMoreL4 = document.createElement('div');
			modalMoreL4.setAttribute('class', 'modal-header');

			let modalMoreL5 = document.createElement('h5');
			modalMoreL5.setAttribute('class', 'modal-title');
			modalMoreL5.setAttribute('id', 'modalLongTitle');
			modalMoreL5.textContent = splitStr[1].normalize('NFD').replace(/[\u0300-\u036f]/g, ""); //Title movie.

			let modalMoreL6 = document.createElement('button');
			modalMoreL6.setAttribute('type', 'button');
			modalMoreL6.setAttribute('class', 'close');
			modalMoreL6.setAttribute('data-dismiss', 'modal');
			modalMoreL6.setAttribute('aria-label', 'Close');
 
			let modalMoreL7 = document.createElement('span');
			modalMoreL7.setAttribute('aria-hidden', 'true');
			modalMoreL7.textContent = 'x';

			let modalMoreL8 = document.createElement('div');
			modalMoreL8.setAttribute('class', 'modal-body');

			modalMoreL6.appendChild(modalMoreL7);		
			modalMoreL4.appendChild(modalMoreL5);
			modalMoreL4.appendChild(modalMoreL6);
			modalMoreL3.appendChild(modalMoreL4);
			let modalHeader = modalMoreL2.appendChild(modalMoreL3);
			modalMore.appendChild(modalMoreL2);
			app.appendChild(modalMore);
 

			// Content here (modalMoreL8).
			modalHeader.append(modalMoreL8);
 
			let bodyModalL1 = document.createElement('h5');
			bodyModalL1.setAttribute('class', 'modal-plot');
			bodyModalL1.setAttribute('id', 'modal-plot');
			bodyModalL1.textContent = splitStr[12].normalize('NFD').replace(/[\u0300-\u036f]/g, ""); //Plot.

			let bodyModalL2 = document.createElement('h6');
			bodyModalL2.setAttribute('class', 'modal-actors');
			bodyModalL2.setAttribute('id', 'modal-actors');
			bodyModalL2.textContent = 'Actors: '+splitStr[11].replaceAll(',', ', '); //Actors movie.

			let bodyModalL3 = document.createElement('h6');
			bodyModalL3.setAttribute('class', 'modal-directors');
			bodyModalL3.setAttribute('id', 'modal-directors');
			bodyModalL3.textContent = 'Directors: '+splitStr[10].replaceAll(',', ', '); //Directors movie.				
			modalMoreL8.appendChild(bodyModalL1);
			modalMoreL8.appendChild(bodyModalL2);
			modalMoreL8.appendChild(bodyModalL3);
		}	

	} else {
 
		const countResultsInit = document.getElementById('countResults')
		$(countResultsInit).css('display','none');

		const card = document.createElement('div');
		card.setAttribute('class', 'card notes');
		//card.setAttribute('class', 'notes');
		//Div Parent.
		const divParentDetails = document.createElement('div');
		divParentDetails.setAttribute('class', 'divParentDetails');
		//First img.
		// const iMovie = document.createElement('img');
		// iMovie.setAttribute('class', 'firstImg');
		// iMovie.src = 'icon/bkgd.jpg'; 
   
		const inititleMovie = document.createElement('h4');
		inititleMovie.setAttribute('class', 'initial-title');
		inititleMovie.textContent = "Not Found";

		container.appendChild(card);
   
		//First img
		card.appendChild(inititleMovie);
  
		let divParent = card.appendChild(divParentDetails);

		const notFound = document.createElement('p');
		notFound.setAttribute('class', 'pNotes');
		notFound.textContent = ' Search not found on Netflix :(';
		
		divParent.appendChild(notFound);

	}

	//Adjust footer.
	adjustFooter(results);

	// Hiden loading screen.
	$(".spinner").css("display", "none");

	// Enable buttons again.		
	$('.searchBtn').removeAttr('disabled');
	$('.filterMaster').removeAttr('disabled');
	$('.checkbox').removeAttr('disabled');

}

function adjustFooter(results) {
	//Add adjust to footer and when find 1 to 4 results.
	if(results <= 4 && $(window).width() > 768 && $(window).height() > 768) {
		$(".divFooter").css("position", "absolute");
		$(".container").css("display", "flex");
		$(".container").css("justify-content", "center");
	} else if(results <= 4 && $(window).height() === 768) {
		//To screen 1366x768
		$(".divFooter").css("position", "relative");
		$(".container").css("display", "flex");
		$(".container").css("justify-content", "center");
	}else{
		$(".divFooter").css("position", "relative"); 
	}
}

for (const option of document.querySelectorAll(".custom-option-top")) {
    option.addEventListener('click', function() {
        if (!this.classList.contains('selected')) {
            this.parentNode.querySelector('.custom-option-top.selected').classList.remove('selected');
            this.classList.add('selected');
            this.closest('.custom-select-top').querySelector('.custom-select__trigger-top span').textContent = this.textContent;
        }
    })
}
 
for (const dropdown of document.querySelectorAll(".custom-select-wrapper-top")) {
    dropdown.addEventListener('click', function() {
        this.querySelector('.custom-select-top').classList.toggle('open');
    })
}
 
window.addEventListener('click', function(e) {
    for (const select of document.querySelectorAll('.custom-select-top')) {
        if (!select.contains(e.target)) {
            select.classList.remove('open');
        }
    }
});
 

//Poor automplete.
const autoComSearchBtn =["Eleanor Tomlinson", "John Lynch"];

// Autocomplete.
$(document).ready(function(){

	autoComplete = autoComSearchBtn;

    // Constructing the suggestion engine
    var autoComplete = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: autoComplete
    });
    
    // Initializing the typeahead
    $('.typeahead').typeahead({
        hint: true,
        highlight: true, /* Enable substring highlighting */
        minLength: 1 /* Specify minimum characters required for showing result */
    },
    {
        name: 'autoComplete',
        source: autoComplete
    });

	menuToggleCLone = $('#menuToggle').clone();
	
});  
