<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Find all movie and tv show of Netflix. Search by note, time, year, country, however you want or search for your favorite actor, director or title. All results are sorted desc IMDB note.">
 		<meta name="keywords" content="Netflix BR movies updated weekly. Search by imdb note, rottentomatoes, time, year, country, however.">
		<link rel="icon" type="image/png" href="icon/oscar.png"/>
		<title>Movie Filter</title>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=G-M2QVV73RMZ"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', 'G-M2QVV73RMZ');
		</script>
	</head>
	<body>
		<div class='title'>
		<h1>Find all Movies of Netflix Brazil (update Set/06/2021)</h1>


		<form method="POST" id="form-search">
		 
		<div class='updates'>
			<div class="top-filters">
				<!-- <button type="button" class="top-filters-btn">Top Filters</button> -->

				<nav role="navigation" id="navigation">
					<div id="menuToggle" class='menuToggle'>
						<input type="checkbox" class="checkbox"/>
						<span></span>
						<span></span>
						<span></span>
						<ul class="menu" id="menu">
							<li>Top 200 Movies</li>
							<hr>
							<li>Top Movie Actions</li>
							<hr>
							<li>Top Movie Comedy (~105min)</li>
							<hr>
							<li>Top 200 TV Show</li>
							<hr>	
							<li>Top TV Show Actions</li>
							<hr>
							<li>Top TV Show Comedy</li>
							<hr>												
						</ul>
					</div>
				</nav>

			</div>	
			 
			<!-- <h6 class='updateText'>Updates on friday  
			<a href='https://github.com/users/' target='_blank'>Bug Track</a></h6> -->
			 
		</div>
			<div class="filterMasterParent">

			
 
				<select class="selectpicker custom-options" title="Genre" data-live-search="true">
					<option value="any" selected>Any Genre</option>
					<option value="Animation">Animation</option>
					<option value="Documentary">Documentary</option>
					<option value="Short">Short</option>
					<option value="Reality-TV">Reality-TV</option>
					<option value="Action">Action</option>
					<option value="Game-Show">Game-Show</option>
					<option value="Biography">Biography</option>
					<option value="Talk-Show">Talk-Show</option>
					<option value="Adventure">Adventure</option>
					<option value="News">News</option>
					<option value="Musical">Musical</option>
					<option value="Crime">Crime</option>
					<option value="Sport">Sport</option>
					<option value="Western">Western</option>
					<option value="Music">Music</option>
					<option value="War">War</option>
					<option value="Mystery">Mystery</option>
					<option value="History">History</option>
					<option value="Horror">Horror</option>
					<option value="Comedy">Comedy</option>
					<option value="Fantasy">Fantasy</option>
					<option value="Romance">Romance</option>
					<option value="Sci-Fi">Sci-Fi</option>
					<option value="Thriller">Thriller</option>
					<option value="Drama">Drama</option>
					<option value="Family">Family</option>
				</select>
 
				<select class="selectpicker custom-options" title="Run Time" data-live-search="true">
					<option value="any" selected>Any Min</option>					
					<option value="10~30">10 to 30</option>
					<option value="30~45">30 to 45</option>
					<option value="45~50">45 to 50</option>
					<option value="50~60">50 to 60</option>
					<option value="60~85">60 to 85</option>
					<option value="60~90">60 to 90</option>
					<option value="60~105">60 to 105</option>
					<option value="60~120">60 to 120</option>
					<option value="85~100">85 to 100</option>
					<option value="100~120">100 to 120</option>
					<option value="120~160">120 to 160</option>
					<option value="1~10">1 to 10</option>
				</select>

				<select class="selectpicker custom-options" title="Year" data-live-search="true">
					<option value="any"selected>Any Year</option>
					<option value="2021">2021</option>
					<option value="2020">2020</option>
					<option value="2019">2019</option>
					<option value="2018">2018</option>
					<option value="2017">2017</option>
					<option value="2016">2016</option>
					<option value="2015">2015</option>
					<option value="2014">2014</option>
					<option value="2013">2013</option>
					<option value="2012">2012</option>
					<option value="2011">2011</option>
					<option value="2010">2010</option>
					<option value="2009">2009</option>
					<option value="2008">2008</option>
					<option value="2007">2007</option>
					<option value="2006">2006</option>
					<option value="2005">2005</option>
					<option value="2004">2004</option>
					<option value="2003">2003</option>
					<option value="2002">2002</option>
					<option value="2001">2001</option>
					<option value="2000">2000</option>
					<option value="1999">1999</option>
					<option value="1998">1998</option>
					<option value="1997">1997</option>
					<option value="1996">1996</option>
					<option value="1995">1995</option>
					<option value="1994">1994</option>
					<option value="1993">1993</option>
					<option value="1991">1991</option>
					<option value="1990">1990</option>
					<option value="1987">1987</option>
					<option value="1986">1986</option>
					<option value="1984">1984</option>
					<option value="1983">1983</option>
					<option value="1981">1981</option>
					<option value="1974">1974</option>
					<option value="1973">1973</option>
					<option value="1972">1972</option>
					<option value="1969">1969</option>
				</select>
 
				<select class="selectpicker custom-options" title="Type" data-live-search="true">
					<option value="any"selected>Any Type</option>
					<option value="movie">Movie</option>
					<option value="series">TV Show</option>
				</select>

				<select class="selectpicker custom-options" title="Country" data-live-search="true">
					<option value="any"selected>Any Country</option>
					<option value="USA">USA</option>
					<option value="Canada">Canada</option>
					<option value="UK">UK</option>
					<option value="Germany">Germany</option>
					<option value="France">France</option>
					<option value="India">India</option>
					<option value="Russia">Russia</option>
					<option value="Ukraine">Ukraine</option>
					<option value="Italy">Italy</option>
					<option value="Brazil">Brazil</option>
					<option value="United Arab Emirates">United Arab Emirates</option>
					<option value="South Korea">South Korea</option>
					<option value="Angola">Angola</option>
					<option value="Argentina">Argentina</option>
					<option value="Australia">Australia</option>
					<option value="Austria">Austria</option>
					<option value="Bahamas">Bahamas</option>
					<option value="Belarus">Belarus</option>
					<option value="Belgium">Belgium</option>
					<option value="Bermuda">Bermuda</option>
					<option value="Bulgaria">Bulgaria</option>
					<option value="Cambodia">Cambodia</option>
					<option value="Canada France">Canada France</option>
					<option value="Chile">Chile</option>
					<option value="China">China</option>
					<option value="Colombia">Colombia</option>
					<option value="Congo">Congo</option>
					<option value="Croatia">Croatia</option>
					<option value="Cyprus">Cyprus</option>
					<option value="Czech Republic">Czech Republic</option>
					<option value="Denmark">Denmark</option>
					<option value="Ecuador">Ecuador</option>
					<option value="Egypt">Egypt</option>
					<option value="Finland">Finland</option>
					<option value="Georgia">Georgia</option>
					<option value="Ghana">Ghana</option>
					<option value="Greece">Greece</option>
					<option value="Guatemala">Guatemala</option>
					<option value="Hong Kong">Hong Kong</option>
					<option value="Hungary">Hungary</option>
					<option value="Iceland">Iceland</option>
					<option value="Indonesia">Indonesia</option>
					<option value="Iran">Iran</option>
					<option value="Ireland">Ireland</option>
					<option value="Israel">Israel</option>
					<option value="Japan">Japan</option>
					<option value="Jordan">Jordan</option>
					<option value="Kenya">Kenya</option>
					<option value="Korea">Korea</option>
					<option value="Kuwait">Kuwait</option>
					<option value="Lebanon">Lebanon</option>
					<option value="Luxembourg">Luxembourg</option>
					<option value="Malaysia">Malaysia</option>
					<option value="Malta">Malta</option>
					<option value="Mauritius">Mauritius</option>
					<option value="Mexico">Mexico</option>
					<option value="Monaco">Monaco</option>
					<option value="Mongolia">Mongolia</option>
					<option value="Morocco">Morocco</option>
					<option value="Namibia">Namibia</option>
					<option value="Netherlands">Netherlands</option>
					<option value="New Zealand">New Zealand</option>
					<option value="Nigeria">Nigeria</option>
					<option value="North Korea">North Korea</option>
					<option value="Norway">Norway</option>
					<option value="Paraguay">Paraguay</option>
					<option value="Peru">Peru</option>
					<option value="Philippines">Philippines</option>
					<option value="Poland">Poland</option>
					<option value="Portugal">Portugal</option>
					<option value="Puerto Rico">Puerto Rico</option>
					<option value="Qatar">Qatar</option>
					<option value="Romania">Romania</option>
					<option value="Saudi Arabia">Saudi Arabia</option>
					<option value="Senegal">Senegal</option>
					<option value="Serbia">Serbia</option>
					<option value="Singapore">Singapore</option>
					<option value="South Africa">South Africa</option>
					<option value="Spain">Spain</option>
					<option value="Sweden">Sweden</option>
					<option value="Switzerland">Switzerland</option>
					<option value="Syria">Syria</option>
					<option value="Taiwan">Taiwan</option>
					<option value="Thailand">Thailand</option>
					<option value="Turkey">Turkey</option>
					<option value="Uruguay">Uruguay</option>
					<option value="Vietnam">Vietnam</option>
					<option value="West Germany">West Germany</option>
					<option value="Zimbabwe">Zimbabwe</option>
				</select>

				<select class="selectpicker custom-options" title="IMDB" data-live-search="true">
					<option value="any" selected>Any IMDB</option>
					<option value="50">1 to 50</option>
					<option value="60"> > 60</option>
					<option value="70"> > 70</option>
					<option value="80"> > 80</option>
					<option value="90"> > 90</option>

				</select>

				<select class="selectpicker custom-options" title="Rotten" data-live-search="true">
					<option value="any" selected>Any Rotten</option>
					<option value="50">1 to 50</option>
					<option value="60"> > 60</option>
					<option value="70"> > 70</option>
					<option value="80"> > 80</option>
					<option value="90"> > 90</option>
				</select>	

				<select class="selectpicker custom-options" title="Metascore" data-live-search="true">
					<option value="any"selected>Any Metascore</option>
					<option value="50">1 to 50</option>
					<option value="60"> > 60</option>
					<option value="70"> > 70</option>
					<option value="80"> > 80</option>
					<option value="90"> > 90</option>
				</select>				

				<div class="custom-button">
					<button type="button" class="button filterMaster">Search</button>
				</div>	
			</div>

			<div class="custom-select-wrapper-top first-input">
				<div class="custom-select-top">
					<div class="custom-select__trigger-top"><span>Title:</span>
						<div class="arrow"></div>
					</div>
					<div class="custom-options-top">
						<span class="custom-option-top selected" data-value="title">Title:</span>
						<span class="custom-option-top" data-value="Actor">Actor:</span>
						<span class="custom-option-top" data-value="director">Director:</span>
					</div>
				</div>
				<div class="autocomplete">
					<input class="typeahead tt-query inputAutoComp" id="inputAutoComp" type="text" name="inputAutoComp" placeholder="Name of movie/tv show" autocomplete="off" spellcheck="false">
				</div>
				<div class="custom-button">
					<button type="button" class="button searchBtn">Search</button>
				</div>	
			</div>	
			<div class="spinner">
				<div class="d-flex justify-content-center">
					<div class="spinner-border" role="status">
						<span class="sr-only">Loading...</span>
					</div>
				</div>
			</div>	
		</form>
 
		<div id='root' class='root'>
			<div class='container' id='container'> </div>
			<div class='countResults' id='countResults'></div>
		</div>

		<footer class="containerParent" id="containerParent">
			<div class="text-center divFooter">
				<span>© <?=date('Y')?> Copyright: rgto</span>
			</div>
		</footer>
		<link href="https://fonts.googleapis.com/css?family=Dosis:400,700" rel="stylesheet">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.15.0/umd/popper.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.9/js/bootstrap-select.min.js"></script>
		<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" />
		<!--<link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">-->
		<link rel="stylesheet" href="css/style.css?ver=2.7">
		<link rel="stylesheet" href="css/bootstrap-select.min.css?ver=2.6"/>
		<link href="css/jquery-ui.css" rel ="Stylesheet">
		<script type="text/javascript" src="js/script.js?ver=2.6"></script>
		<script type="text/javascript" src="js/typeahead.bundle.js"></script>
		
	</body>
</html>